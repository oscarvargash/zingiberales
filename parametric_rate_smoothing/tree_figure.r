setwd("/Users/oscar/Dropbox/COLLABORATION/Zingiberales/parametric_rate_smoothing")
dir()

library(ape)
library(maps)
library(phytools)
library(phangorn)
library(ggtree)


tree <- read.tree("zingiberales_chronogram_1.no.tre")

species <- c("Heliconia_wagneriana","Heliconia_irrasa","Heliconia_umbrophila","Heliconia_imbricata","Heliconia_psittacorum","Heliconia_pogonantha","Heliconia_mariae","Calathea_lutea","Calathea_crotalifera","Calathea_lasiostachya","Pleiostachya_pruinosa","Goeppertia_micans","Goeppertia_marantifolia","Goeppertia_inocephala","Goeppertia_gymnocarpa","Costus_malortieanus","Costus_scaber","Costus_bracteatus","Costus_pulverulentus","Costus_laevis","Renealmia_alpinia","Renealmia_cernua","Alpinia_purpurata","Etlingera_elatior","Kaempferia_rotunda","Zingiber_spectabile","Hedychium_coronarium")

tree_red <- drop.tip(tree, tree$tip.label[-match(species, tree$tip.label)])

p <- ggtree(tree_red, ladderize = T, right = T) + geom_tiplab()
p + theme_tree2()

write.tree(tree_red, file="chronogram_sampling.tre")