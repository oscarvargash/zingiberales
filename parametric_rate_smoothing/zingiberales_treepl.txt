treefile = RAxML_bestTree.tre
smooth = 1000
numsites= 22237
mrca = zingiberales Musa_exotica Costus_arabicus
min = zingiberales 69
max = zingiberales 86
mrca = zingiberaceae Zingiber_officinale Maranta_bicolor
min = zingiberaceae 63
max = zingiberaceae 76
outfile = zingiberales_chronogram_1.tre
thorough
#prime
opt = 1
optad = 1
moredetailad
optcvad = 1
moredetailcvad