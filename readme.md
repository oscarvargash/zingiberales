# Evolutionary history constrains heat tolerance of native and exotic tropical Zingiberales  #

This is a partial repository for the paper "Evolutionary history constrains heat tolerance of native and exotic tropical Zingiberales" published in Functional Ecology.

This repository contains the phylogenetic analysis performed to obtain a comprehensive tree of the order Zingiberales based on published GenBank sequences. The repository is organized in two main folders.

The raxml_analysis folder contains the matrix used to infer the tree with the ITS, matK, and trnL markers. These markers were mined out of genbank using PyPHLAWD https://besjournals.onlinelibrary.wiley.com/doi/full/10.1111/2041-210X.13096


The parametric_rate_smoothing folder contains all the files used to calibrate inferred in the phylogetnic ananlysis.
